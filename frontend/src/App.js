import { BooksList } from "./components/BooksList/BooksList";
import "./components/BooksList/BooksList.css";

function App() {
  return (
    <div className="App">
      <div className="wrapper">
        <h1 className="title">Books reading list</h1>
        <BooksList />
      </div>
    </div>
  );
}

export default App;
