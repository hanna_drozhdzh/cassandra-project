import React, { useState, useEffect } from "react";
import { getBooks } from "../../api";

import { AddBookForm } from "../AddBookForm/AddBookForm";
import { Book } from "../Book/Book";

export const BooksList = () => {
  const [books, setBooks] = useState([]);
  const [showForm, setShowForm] = useState(false);

  const handleAddBookClick = () => {
    setShowForm(!showForm); // Show the form when the button is clicked
  };

  useEffect(() => {
    getBooks().then((books) => setBooks(books));
  }, []);

  return (
    <div >
      <div>
        {books.length === 0 && <p>No books found.</p>}
        <ul className="list">
          {books.map((book) => (
            <li key={book.book_id}>
              <Book name={book.name} id={book.book_id} setBooks={setBooks} />
            </li>
          ))}
        </ul>
        <button className="addButton" onClick={handleAddBookClick}>Add Book</button>
      </div>
      {showForm && (
        <AddBookForm setBooks={setBooks} setShowForm={setShowForm} />
      )}
    </div>
  );
};
