import { deleteBook, editBook, getBooks, getBook } from "../../api";
import "./Book.css";
export const Book = ({ name, id, setBooks }) => {
  const handleDelete = async (book_id) => {
    try {
      alert(`Book ${name} deleted successfully`);
      await deleteBook(book_id);
      setBooks(await getBooks());
    } catch (error) {
      console.error(`Error deleting book with ID ${id}:`, error);
      throw error;
    }
  };

  const handleEdit = async (id) => {
    const book = await getBook(id);
    const name = book.name;

    const newName = prompt("Enter the new name for the book:", name);
    if (newName === null || newName.trim() === "") {
      // User canceled or entered empty name
      return;
    }
    try {
      alert(`Book ${name} edited successfully to ${newName}`);
      await editBook(id, newName);
      setBooks(await getBooks());
    } catch (error) {
      console.error(`Error editing book with ID ${id}:`, error);
      throw error;
    }
  };

  return (
    <div>
      <p className="bookItem">{name}</p>
      <div className="buttons">
        <button className="button" onClick={() => handleDelete(id)}>
          Delete
        </button>
        <button className="button" onClick={() => handleEdit(id)}>
          Edit
        </button>
      </div>
    </div>
  );
};
