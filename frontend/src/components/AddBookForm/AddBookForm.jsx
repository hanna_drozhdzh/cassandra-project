import React, { useState } from "react";
import { addBook, getBooks } from "../../api";
import "./AddBookForm.css";
export const AddBookForm = ({ setBooks, setShowForm }) => {
  // State variables to hold book information
  const [name, setName] = useState("");

  // Event handler to update state when input values change
  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  // Function to handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    const book_id = Date.now().toString();
    try {
      alert(`Book ${name} added successfully`);
      await addBook(name, book_id);
      setBooks(await getBooks());
      setShowForm(false);
    } catch (error) {
      console.error("Error adding book:", error);
    }
  };

  return (
    <form className="form" onSubmit={handleSubmit}>
      <label className="label" htmlFor="name">
        Title:
      </label>
      <br />
      <input
        className="input"
        type="text"
        id="name"
        value={name}
        onChange={handleNameChange}
        required
        autoComplete="off"
      />
      <br />
      <button className="submitButton" type="submit">
        Submit
      </button>
    </form>
  );
};
