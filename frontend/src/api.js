import axios from "axios";

const BASE_URL = "http://localhost:3000"; // Assuming your backend server is running on this URL

export const getBooks = async () => {
  try {
    const response = await axios.get(`${BASE_URL}/books`);
    return response.data;
  } catch (error) {
    console.error("Error fetching books:", error);
    throw error;
  }
};

export const addBook = async (name, book_id) => {
  try {
    book_id = book_id.toString();
    const response = await axios.post(`${BASE_URL}/books`, {
      book_id,
      name,
    });
    return response.data;
  } catch (error) {
    console.error("Error adding book:", error);
    throw error;
  }
};

export const editBook = async (id, name) => {
  try {
    const response = await axios.put(`${BASE_URL}/books/'${id}'`, {
      name,
    });
    return response.data;
  } catch (error) {
    console.error(`Error editing book with ID ${id}:`, error);
    throw error;
  }
};

export const deleteBook = async (id) => {
  try {
    const response = await axios.delete(`${BASE_URL}/books/${id}`); // Remove single quotes around id
    return response.data;
  } catch (error) {
    console.error(`Error deleting book with ID ${id}:`, error);
    throw error;
  }
};

export const getBook = async (id) => {
  try {
    const response = await axios.get(`${BASE_URL}/books/'${id}'`);
    return response.data;
  } catch (error) {
    console.error(`Error fetching book with ID ${id}:`, error);
    throw error;
  }
};
