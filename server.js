process.env.GOOGLE_APPLICATION_CREDENTIALS = "./certs.json";

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { BigQuery } = require("@google-cloud/bigquery");

const app = express();
const bigquery = new BigQuery();

app.use(bodyParser.json());
app.use(cors());

const PORT = process.env.PORT || 3000;

const tableName = "alpine-inkwell-418312.Demo.books";

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

// GET all books
app.get("/books", async (req, res) => {
  try {
    const query = `SELECT * FROM ${tableName}`;
    const [rows] = await bigquery.query(query);
    res.json(rows);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});

// GET a specific book by ID
app.get("/books/:id", async (req, res) => {
  try {
    const query = `SELECT * FROM ${tableName} WHERE book_id = ${req.params.id}`;
    const [rows] = await bigquery.query(query);
    res.json(rows[0]);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});

// POST a new book
app.post("/books", async (req, res) => {
  try {
    const { book_id, name } = req.body;
    const query = `INSERT INTO ${tableName} (book_id, name) VALUES ('${book_id}', '${name}')`;
    await bigquery.query(query);
    res.status(201).send("Book added successfully");
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});

// PUT update an existing book
app.put("/books/:id", async (req, res) => {
  try {
    const { name } = req.body;
    const query = `UPDATE ${tableName} SET name = '${name}' WHERE book_id = ${req.params.id}`;
    await bigquery.query(query);
    res.status(200).send("Book updated successfully");
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});

// DELETE a book by ID
app.delete("/books/:id", async (req, res) => {
  try {
    const query = `DELETE FROM ${tableName} WHERE book_id = '${req.params.id}'`; // Wrap id in single quotes
    await bigquery.query(query);
    res.status(204).send();
  } catch (error) {
    console.error("Error:", error);
    res.status(500).send("Internal Server Error");
  }
});
